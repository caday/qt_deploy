#! python3
#encoding: utf-8

from __future__ import print_function, unicode_literals

import os
import sys
import codecs
import locale
import argparse
import shutil
import glob
import filecmp
import ntpath
import subprocess
import re

from collections import MutableMapping, Set

SCRIPT_DIR = os.path.realpath(os.path.dirname(__file__))
SCRIPT_NAME = os.path.basename(__file__)

ARCH_OPTIONS = ['x86', 'x64']
BUILD_TYPE_OPTIONS = ['Release', 'Debug', 'Profile']
BUILD_TYPE_TO_SUBTYPE = [('Release', 'release'), ('Debug', 'debug'), ('Profile', 'release')]
BUILD_SUBTYPE_OPTIONS = ['release', 'debug']

QT_SCRIPT_NAME = 'qtenv2.bat'

REDIST_32_FILE_PATTERN = r"vc(\_)?redist[\_\.]x86\.exe"
REDIST_64_FILE_PATTERN = r"vc(\_)?redist[\_\.]x64\.exe"
REDIST_32_OUT_FILE = 'vcredist_x86.exe'
REDIST_64_OUT_FILE = 'vcredist_x64.exe'

class ConsoleColors:
    DEBUG = '\033[36;40;1m'
    INFO = '\033[37;40;1m'
    WARNING = '\033[33;40;1m'
    ERROR = '\033[31;40;1m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

class Console(object):

    TABS = 0
    TABS_STR = '   '
    USE_COLORS = False
    ENCODING = 'cp852'
    LAST_CHAR = chr(0)

    @staticmethod
    def ColoredMessage(message, color):
        return '{0}{1}{2}'.format(color, message, ConsoleColors.END)

    @staticmethod
    def Print(message, pre=None, tabs=0, color=None, nonl=False, **kwargs):

        kwargs.setdefault('flush', True)
        kwargs.setdefault('end', '\n')

        message = message.replace('\r\n', '\n')

        if nonl:
            message = re.sub(r'\n+', '\n', message)

        message_list = message.split('\n')

        if len(message_list) > 1:
            for i_message in message_list:
                Console.Print(i_message, pre=pre, tabs=tabs, color=color, nonl=nonl, **kwargs)
            return

        Console.TABS += tabs

        if pre:
            message = '[{0}] {1}'.format(pre, message)

        if Console.LAST_CHAR == '\n':
            for i in range(Console.TABS):
                message = Console.TABS_STR + message

        if len(kwargs['end']) != 0:
            Console.LAST_CHAR = kwargs['end'][-1]
        elif len(message) != 0:
            Console.LAST_CHAR = message[-1]

        # print('Console.LAST_CHAR={0}'.format(repr(Console.LAST_CHAR)))

        Console.TABS -= tabs

        if color and Console.USE_COLORS:
            message = Console.ColoredMessage(message, color)

        print(message, **kwargs)

    @staticmethod
    def PrintWarning(message, **kwargs):
        Console.Print(message, 'Warning', color=ConsoleColors.WARNING, **kwargs)
    @staticmethod
    def PrintError(message, **kwargs):
        Console.Print(message, 'Error', color=ConsoleColors.ERROR, **kwargs)
    @staticmethod
    def PrintInfo(message, **kwargs):
        Console.Print(message, 'Info', color=ConsoleColors.INFO, **kwargs)
    @staticmethod
    def PrintDebug(message, **kwargs):
        Console.Print(message, 'Debug', color=ConsoleColors.DEBUG, **kwargs)

    @staticmethod
    def PrintNL(lines=1):
        message = ''
        for i in range(1, lines):
            message += '\n'
        Console.Print(message)

    @staticmethod
    def PrintHelp(text):
        Console.Print(
            'Należy podać {0} w parametrach uruchomieniowych skryptu.\n'
            "Użyj polecenia '{1} -h' aby otrzymać więcej pomocy."
            .format(text, SCRIPT_NAME)
        )

class ScriptConfig(MutableMapping):
    def __init__(self):
        self._storage = dict()

    def __len__(self):
        return len(self._storage)
    def __iter__(self):
        return iter(self._storage)
    def __getitem__(self, key):
        #print('ScriptConfig.__getitem__({0})'.format(key))
        return self._storage.get(key, '')
    def __setitem__(self, key, value):
        #print('ScriptConfig.__setitem__({0}, {1})'.format(key, value))
        self._storage[key] = value
    def __delitem__(self, key):
        #print('ScriptConfig.__delitem__({0})'.format(key))
        del self._storage[key]

    def is_defined(self, var):
        return self._storage.get(var) is not None

    def is_undefined(self, var, silent=False):
        m_defined = self.is_defined(var)
        if not m_defined and not silent:
            self.print_undefined(var)
        return not m_defined

    @staticmethod
    def _check_option(value, option, regex=False):
        if not regex:
            return value == option

        return re.match(option, value, re.I) is not None

    def check(self, var, options, silent=False, regex=False):
        m_check = False

        if not self.is_defined(var):
            return m_check

        value = self[var]

        if type(options) in (list, tuple):
            for option in options:
                m_check = self._check_option(value, option, regex)
                if m_check is True:
                    break
        else:
            m_check = self._check_option(value, options, regex)

        if not m_check and not silent:
            self.print_invalid(var, options)

        return m_check

    def print_undefined(self, var):
        Console.PrintError('Wartość {0} nie została skonfigurowana.'.format(var))

    def print_invalid(self, var, options=None, is_path=False):
        Console.PrintError('Wartość {0} została źle skonfigurowana.'.format(var))
        if is_path is True:
            Console.Print('Ścieżka "{0}" jest nieprawidłowa.'.format(self[var]))
        if options is not None:
            Console.Print('Możliwe opcje: {0}.'.format(', '.join(options)))

    def fixpath(self, var):
        if self.is_defined(var):
            self[var] = os.path.realpath(self[var])

    def is_dir(self, var):
        return os.path.isdir(self[var])

    def print_param(self, var):
        Console.Print("config['{0}'] = '{1}'".format(var, self[var]))

    def print_params(self):
        f_items = sorted(self._storage.items(), key=lambda x: x[0])
        for key, value in f_items:
            Console.Print("config['{0}'] = '{1}'".format(key, value))
        Console.PrintNL(1)

    __hash__ = Set._hash

class ShellExecuter(object):
    def __init__(self):
        self.commands = ''
        self.pipe = None
        self.out = b''
        self.err = b''

    def add(self, command):
        self.commands += command + '\n'

    def execute(self, config):
        self.pipe = subprocess.Popen(
            "cmd.exe",
            shell = True,
            cwd = SCRIPT_DIR,
            stdin = subprocess.PIPE,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE
        )

        self.out, self.err = self.pipe.communicate(
            self.commands.encode(Console.ENCODING)
        )

    def get_output(self):
        return self.out.decode(Console.ENCODING)

    def get_errors(self):
        return self.err.decode(Console.ENCODING)

def join_path(path, *paths):
    return os.path.realpath(os.path.join(path, *paths))

def file_exist(file_path):
    return os.path.isfile(file_path)

def dir_exist(dir_path):
    return os.path.isdir(dir_path)

def get_default_msvc2017_dir():
    PROGRAM_FILES_32 = os.getenv('ProgramFiles(x86)')

    msvc_dir = "Microsoft Visual Studio/2017/Community/VC/Auxiliary/Build"
    msvc_dir_path = join_path(PROGRAM_FILES_32, msvc_dir)

    if dir_exist(msvc_dir_path):
        return msvc_dir_path

    msvc_dir = "Microsoft Visual Studio/2017/BuildTools/VC/Auxiliary/Build"
    msvc_dir_path = join_path(PROGRAM_FILES_32, msvc_dir)

    if dir_exist(msvc_dir_path):
        return msvc_dir_path

    return None

def copy_file(file_path_from, file_path_to):
    file_name = os.path.basename(file_path_to)

    if os.path.isfile(file_path_to):
        if filecmp.cmp(file_path_from, file_path_to):
            Console.Print('Plik {} jest identyczny, nie ma potrzeby kopiowania.'.format(file_name))
            return False

    Console.Print('Kopiowanie {}...'.format(file_name))
    shutil.copy(file_path_from, file_path_to)
    return True

def move_file(file_path_from, dest):
    file_name = os.path.basename(file_path_from)
    Console.Print('Przenoszenie {}...'.format(file_name))
    shutil.copy(file_path_from, dest)
    os.remove(file_path_from)
    return True

def glob_copy_file(dir_path, file_name, dest_path):
    files = glob.glob(dir_path + "/*/" + file_name)

    if len(files) >= 1:
        dir_name = str(os.path.dirname(files[0]))[len(dir_path)+1:]
        Console.Print('Znaleziono plik {0} w podkatalogu {1}.'.format(file_name, dir_name))
        copy_file(files[0], dest_path)
        return True

    return False

def file_exist_glob(dir_path, file_name_regex):
    files = glob.glob(dir_path + "/" + file_name_regex)
    return len(files) != 0

def get_file_name_glob(dir_path, file_name_regex):
    files = glob.glob(os.path.join(dir_path, file_name_regex))
    if len(files) != 0:
        file_name = os.path.basename(files[0])
        return os.path.splitext(file_name)[0]
    return None

def get_file_path_glob(dir_path, file_name_glob):
    files = get_files_glob(dir_path, file_name_glob)
    if len(files) != 0:
        return files[0]
    return None

def get_files_glob(dir_path, file_name_glob):
    return glob.glob(dir_path + '/**/' + file_name_glob, recursive=True)

def make_dir(dir_path):
    dir_name = os.path.basename(dir_path)

    if os.path.isdir(dir_path):
        Console.Print('Katalog {0} już istnieje.'.format(dir_name))
        return

    Console.Print('Tworzenie katalogu {0}...'.format(dir_name))
    os.mkdir(dir_path)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-pn', '--project-name', help = 'Nazwa projektu', type = str, dest = 'PROJECT_NAME')
    parser.add_argument('-p', '--project-path', help = 'Ścieżka do katalogu projektu', type = str, dest = 'PROJECT_PATH')
    parser.add_argument('-a', '--arch', help = 'Architektura kompilatora', type = str, dest = 'ARCH')
    parser.add_argument('-b', '--build-path', help = 'Ścieżka do katalogu build', type = str, dest = 'BUILD_PATH')
    parser.add_argument('-bd', '--build-dir', help = 'Katalog build (np. build)', type = str, dest = 'BUILD_DIR')
    parser.add_argument('-k', '--build-kit', help = 'Kompilator (MSVC, MinGW)', type = str, dest = 'BUILD_KIT')
    parser.add_argument('-t', '--build-type', help = 'Typ kompilacji (Release, Debug, Profile)', type = str, dest = 'BUILD_TYPE')
    parser.add_argument('-s', '--build-subtype', help = 'Subtyp kompilacji (release, debug)', type = str, dest = 'BUILD_SUBTYPE')
    parser.add_argument('-r', '--res-dir-name', help = 'Katalog z zasobami (np. resources)', type = str, dest = 'RES_DIR_NAME')
    parser.add_argument('-rp', '--res-dir-path', help = 'Ścieżka do katalogu z zasobami', type = str, dest = 'RES_PATH')
    parser.add_argument('-on', '--out-dir-name', help = 'Nazwa katalogu wynikowego (np. PROJEKT_x86)', type = str, dest = 'OUT_DIR_NAME')
    parser.add_argument('-o', '--out-dir-path', help = 'Ścieżka do katalogu wynikowego', type = str, dest = 'OUT_DIR_PATH')
    parser.add_argument('-qt', '--qt-path', help = 'Ścieżka do katalogu Qt', type = str, dest = 'QT_PATH')
    parser.add_argument('-m', '--msvc-dir-path', help = 'Ścieżka do katalogu ze skryptami Visual Studio', type = str, dest = 'MSVC_DIR_PATH')
    parser.add_argument('-ms', '--msvc-script-name', help = 'Nazwa skryptu Visual Studio (np. vcvarsall.bat)', type = str, dest = 'MSVC_SCRIPT_NAME')
    return parser.parse_args()

def update_config_from_args(config, args):
    for key, value in vars(args).items():
        if value is not None:
            config[key.upper()] = value

def update_config(config, args):

    # config defaults

    config.update({
        'BUILD_DIR': 'build',
        'BUILD_TYPE': 'Release',
        'RES_DIR_NAME': 'resources',
        'REDIST_DIR': 'redist',
    })

    # update config from script parameters

    update_config_from_args(config, args)

    # PROJECT_PATH

    if not config.is_defined('PROJECT_PATH'):
        Console.PrintWarning('Nie podano ścieżki projektu, skrypt użyje domyślnej (jeden katalog w górę).')
        config['PROJECT_PATH'] = join_path(SCRIPT_DIR, '..')

    config.fixpath('PROJECT_PATH')

    if not config.is_dir('PROJECT_PATH'):
        config.print_invalid('PROJECT_PATH', is_path=True)
        return False

    if not file_exist_glob(config['PROJECT_PATH'], '*.pro'):
        Console.PrintWarning('W katalogu projektu nie znaleziono pliku *.pro.')

    # PROJECT_NAME

    if not config.is_defined('PROJECT_NAME'):
        Console.PrintWarning('Nie podano nazwy projektu, zostanie odczytana nazwy z pliku *.pro, jeśli taki istnieje.')
        PROJECT_NAME = get_file_name_glob(config['PROJECT_PATH'], '*.pro')
        if PROJECT_NAME:
            config['PROJECT_NAME'] = PROJECT_NAME
        else:
            Console.PrintWarning('Nie znaleziono pliku *.pro, nazwa projektu zostanie odczytana z PROJECT_PATH.')
            config['PROJECT_NAME'] = os.path.basename(config['PROJECT_PATH'])

    if config.is_undefined('PROJECT_NAME'):
        Console.PrintHelp('nazwę projektu')
        return False

    # BUILD_KIT

    if config.is_undefined('BUILD_KIT'):
        Console.PrintHelp('nazwę kompilatora')
        return False

    # ARCH

    if config.is_undefined('ARCH'):
        # try to find ARCH in BUILD_KIT
        for arch in ARCH_OPTIONS:
            if re.search(arch, config['BUILD_KIT'], re.I):
                config['ARCH'] = arch
                break

    if config.is_undefined('ARCH'):
        Console.PrintHelp('architekturę kompilatora')
        return False

    if not config.check('ARCH', ARCH_OPTIONS):
        return False

    # IS_32_BIT # IS_64_BIT

    config['IS_32_BIT'] = config.check('ARCH', 'x86', silent=True, regex=True)
    config['IS_64_BIT'] = config.check('ARCH', 'x64', silent=True, regex=True)

    # BUILD_DIR

    # IS_MSVC # IS_MINGW

    config['IS_MSVC'] = config.check('BUILD_KIT', 'msvc', silent=True, regex=True)
    config['IS_MINGW'] = config.check('BUILD_KIT', ['mingw', 'gcc'], silent=True, regex=True)

    if not config['IS_MSVC'] and not config['IS_MINGW']:
        Console.PrintWarning('Nie wykryto wspieranego przez skrypt kompilatora.')

    # BUILD_TYPE

    # if not config.check('BUILD_TYPE', BUILD_TYPE_OPTIONS):
    #    return False

    # BUILD_SUBTYPE

    if config.is_defined('BUILD_TYPE'):
        if not config.is_defined('BUILD_SUBTYPE'):
            for item in BUILD_TYPE_TO_SUBTYPE:
                if item[0] == config['BUILD_TYPE']:
                    config['BUILD_SUBTYPE'] = item[1]
                    break

    # QT_PATH

    if config.is_undefined('QT_PATH'):
        Console.PrintHelp('ścieżkę do katalogu Qt')
        return False

    # QT_BIN_PATH

    config['QT_BIN_PATH'] = join_path(config['QT_PATH'], 'bin')

    if not config.is_dir('QT_BIN_PATH'):
        config.print_invalid('QT_BIN_PATH', is_path=True)
        return False

    # QT_SCRIPT_PATH

    config['QT_SCRIPT_PATH'] = join_path(config['QT_BIN_PATH'], QT_SCRIPT_NAME)

    if not file_exist(config['QT_SCRIPT_PATH']):
        Console.PrintWarning('W katalogu Qt bin nie znaleziono skryptu {0}.'.format(QT_SCRIPT_NAME))

    # BUILD_PATH

    if not config.is_defined('BUILD_PATH'):
        Console.PrintWarning('Nie podano ścieżki build, skrypt spróbuje ją odgadnąć.')
        config['BUILD_PATH'] = join_path(
            config['PROJECT_PATH'],
            config['BUILD_DIR'],
            config['BUILD_KIT'],
            config['BUILD_TYPE'],
            config['BUILD_SUBTYPE']
        )

    config.fixpath('BUILD_PATH')

    if not config.is_dir('BUILD_PATH'):
        config.print_invalid('BUILD_PATH', is_path=True)
        return False

    if not file_exist_glob(config['BUILD_PATH'], '*.exe'):
        Console.PrintError('W katalogu build nie znaleziono pliku *.exe.')
        return False

    # RES_DIR_NAME

    # RES_PATH

    if not config.is_defined('RES_PATH'):
        Console.PrintWarning('Nie podano ścieżki do zasobów, skrypt spróbuje ją odgadnąć.')
        config['RES_PATH'] = join_path(
            config['PROJECT_PATH'],
            config['RES_DIR_NAME']
        )

    if not config.is_dir('RES_PATH'):
        Console.PrintWarning('Ścieżka do zasobów jest nieprawidłowa, skrypt użyje ścieżki projektu w zamian.')
        config['RES_PATH'] = config['PROJECT_PATH']

    # OUT_DIR_NAME

    if not config.is_defined('OUT_DIR_NAME'):
        config['OUT_DIR_NAME'] = config['PROJECT_NAME'] + '_' + config['ARCH']

    # OUT_DIR_PATH

    if not config.is_defined('OUT_DIR_PATH'):
        config['OUT_DIR_PATH'] = join_path(
            SCRIPT_DIR,
            config['OUT_DIR_NAME']
        )

    # EXE_NAME

    if not config.is_defined('EXE_NAME'):
        config['EXE_NAME'] = config['PROJECT_NAME'] + ".exe"

    # EXE_PATH

    if not config.is_defined('EXE_PATH'):
        config['EXE_PATH'] = join_path(
            config['BUILD_PATH'],
            config['EXE_NAME']
        )

    if not file_exist(config['EXE_PATH']):
        config.print_invalid('EXE_PATH', is_path=True)
        return False

    # OUT_EXE_PATH

    if not config.is_defined('OUT_EXE_PATH'):
        config['OUT_EXE_PATH'] = join_path(
            config['OUT_DIR_PATH'],
            config['EXE_NAME']
        )

    # ICO_NAME

    if not config.is_defined('ICO_NAME'):
        config['ICO_NAME'] = config['PROJECT_NAME'] + ".ico"

    # OUT_ICO_PATH

    if not config.is_defined('OUT_ICO_PATH'):
        config['OUT_ICO_PATH'] = join_path(
            config['OUT_DIR_PATH'],
            config['ICO_NAME']
        )

    config.fixpath('OUT_ICO_PATH')

    # ICO_PATH

    if not config.is_defined('ICO_PATH'):
        config['ICO_PATH'] = join_path(
            config['RES_PATH'],
            config['ICO_NAME']
        )

        if not file_exist(config['ICO_PATH']):
            Console.PrintWarning('Nie znaleziono pliku ikony w folderze zasobów, przeszukiwanie podkatalogów...')
            icons = get_files_glob(config['RES_PATH'], config['ICO_NAME'])
            for icon in icons:
                if icon != config['OUT_ICO_PATH']:
                    config['ICO_PATH'] = icon
                    break

    # MSVC specific

    if config['IS_MSVC']:

        if not config.is_defined('MSVC_DIR_PATH'):
            Console.PrintWarning('Ścieżka do folderu ze skryptami MSVC nie została podana, '
                                 'skrypt użyje domyślnej ścieżki dla Visual Studio 2017.')

            config['MSVC_DIR_PATH'] = get_default_msvc2017_dir()

        if not config.is_defined('MSVC_SCRIPT_NAME'):
            Console.PrintWarning('Nazwa skryptu MSVC nie została podana, skrypt spróbuje ją odgadnąć.')
            config['MSVC_SCRIPT_NAME'] = 'vcvars{0}.bat'.format(
                '32' if config['IS_32_BIT'] else '64'
            )

        if config.is_defined('MSVC_DIR_PATH'):
            config['MSVC_SCRIPT_PATH'] = join_path(
                config['MSVC_DIR_PATH'],
                config['MSVC_SCRIPT_NAME']
            )

        config['REDIST_DIR_PATH'] = join_path(
            config['PROJECT_PATH'],
            config['REDIST_DIR']
        )

        config['REDIST_OUT_DIR_PATH'] = join_path(
            config['OUT_DIR_PATH'],
            config['REDIST_DIR']
        )

    # ending

    return True

def execute_deploy(config):

    Console.Print("Rozpoczynanie instalacji...")
    Console.PrintNL()

    Console.TABS += 1

    make_dir(config['OUT_DIR_PATH'])

    if file_exist(config['EXE_PATH']):
        copy_file(config['EXE_PATH'], config['OUT_EXE_PATH'])
    else:
        Console.Print('Nie znaleziono pliku {0}!'.format(config['EXE_NAME']))

    if file_exist(config['ICO_PATH']):
        copy_file(config['ICO_PATH'], config['OUT_ICO_PATH'])
    else:
        Console.Print('Ścieżka do pliku ikony jest nieprawidłowa.')

    Console.Print('Uruchomienie WinDeployQt...', end='')

    shell = ShellExecuter()

    shell.add('call "{0}"'.format(config['QT_SCRIPT_PATH']))

    if config['IS_MSVC'] and config.is_defined('MSVC_SCRIPT_PATH'):
        shell.add('call "{0}"'.format(config['MSVC_SCRIPT_PATH']))

    shell.add('windeployqt.exe "{0}"'.format(config['OUT_DIR_PATH']))

    shell.execute(config)

    windeployqt_printed = 0
    windeployqt_output = shell.get_output()
    windeployqt_errors = shell.get_errors()

    if len(windeployqt_output) != 0 and config['PRINT_WINDEPLOYQT']:
        Console.PrintNL()
        Console.Print('WinDeployQt OUTPUT:')
        Console.Print(windeployqt_output, tabs=1, nonl=True)
        windeployqt_printed += 1

    if len(windeployqt_errors) != 0 and config['PRINT_WINDEPLOYQT_ERRORS']:
        if windeployqt_printed == 0:
            Console.PrintNL()
        Console.Print('WinDeployQt ERRORS:')
        Console.Print(windeployqt_errors, tabs=1, nonl=True)
        windeployqt_printed += 1

    if windeployqt_printed == 0:
        Console.Print(' DONE.')
    else:
        Console.Print('Zakończono WinDeployQt.')

    if config['IS_MSVC']:
        redist_file_pattern = REDIST_32_FILE_PATTERN if config['IS_32_BIT'] else REDIST_64_FILE_PATTERN

        redist_files = glob.glob(config['OUT_DIR_PATH'] + '/' + '*.exe')
        redist_files = [f for f in redist_files if re.match(redist_file_pattern, os.path.basename(f), re.I)]

        move_redist = True

        if len(redist_files) == 0:
            move_redist = False
            if dir_exist(config['REDIST_DIR_PATH']):
                redist_files = glob.glob(config['REDIST_DIR_PATH'] + '/' + '*.exe')
                redist_files = [f for f in redist_files if re.match(redist_file_pattern, os.path.basename(f), re.I)]

        if len(redist_files) >= 1:
            redist_filepath = redist_files[0]
            redist_out_name = REDIST_32_OUT_FILE if config['IS_32_BIT'] else REDIST_64_OUT_FILE

            make_dir(config['REDIST_OUT_DIR_PATH'])

            if move_redist:
                move_file(redist_filepath, join_path(config['REDIST_OUT_DIR_PATH'], redist_out_name))
            else:
                copy_file(redist_filepath, join_path(config['REDIST_OUT_DIR_PATH'], redist_out_name))

    Console.TABS -= 1

    Console.PrintNL()
    Console.Print('Skrypt instalacyjny zakończony.')
    Console.PrintNL()

def main():

    Console.PrintNL()
    Console.Print('{0} - skrypt instalacyjny Qt'.format(SCRIPT_NAME))
    Console.Print('Copyright (C) Paweł Stańczak'.format(SCRIPT_NAME))
    Console.PrintNL()

    config = ScriptConfig()
    config['PRINT_WINDEPLOYQT'] = True
    config['PRINT_WINDEPLOYQT_ERRORS'] = True

    args = parse_args()

    config_is_valid = update_config(config, args)

    Console.PrintNL()

    config.print_params()

    if not config_is_valid:
        return

    execute_deploy(config)

if __name__ == "__main__":
    main()
